## Accepting User Input

This project demonstrates how to accept user input in a WatchOS app.

User input can come from three places:

1. Premade suggestions
2. Voice recongition
3. Scribbles

## Class Notes
[https://docs.google.com/document/d/1xknH_55Kl_ApmkRKXRdq6lg-bDcSUeS9vlvAcEVCMK0/edit#](https://docs.google.com/document/d/1xknH_55Kl_ApmkRKXRdq6lg-bDcSUeS9vlvAcEVCMK0/edit#)
