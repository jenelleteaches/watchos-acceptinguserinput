//
//  InterfaceController.swift
//  TextInputDemo WatchKit Extension
//
//  Created by MacStudent on 2019-03-04.
//  Copyright © 2019 MacStudent. All rights reserved.
//

import WatchKit
import Foundation


class InterfaceController: WKInterfaceController {

    
    // MARK: Outlets
    @IBOutlet weak var responseLabel: WKInterfaceLabel!
    
    
    // MARK: Default functions
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    
    // MARK: Button handler
    @IBAction func replyButtonClicked() {
        print("Button pressed!")
        
        
        // 1. When person clicks on button, show them the input UI
        let suggestedResponses = ["In class", "Doing Assignment", "At movies", "Sleeping"]
        presentTextInputController(withSuggestions: suggestedResponses, allowedInputMode: .plain) { (results) in
            
            
            if (results != nil && results!.count > 0) {
                // 2. write your code to process the person's response
                let userResponse = results?.first as? String
                self.responseLabel.setText(userResponse)
            }
        }
        
        
    }
}
